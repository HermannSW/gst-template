/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2019  <<user@hostname.org>>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-redfilter
 *
 * FIXME:Describe redfilter here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! redfilter ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gst/gst.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#include "gstredfilter.h"

GST_DEBUG_CATEGORY_STATIC (gst_red_filter_debug);
#define GST_CAT_DEFAULT gst_red_filter_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SILENT
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("ANY")
    );

#define gst_red_filter_parent_class parent_class
G_DEFINE_TYPE (GstRedFilter, gst_red_filter, GST_TYPE_ELEMENT);

static void gst_red_filter_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_red_filter_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_red_filter_sink_event (GstPad * pad, GstObject * parent, GstEvent * event);
static GstFlowReturn gst_red_filter_chain (GstPad * pad, GstObject * parent, GstBuffer * buf);

/* GObject vmethod implementations */

/* initialize the redfilter's class */
static void
gst_red_filter_class_init (GstRedFilterClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  gobject_class->set_property = gst_red_filter_set_property;
  gobject_class->get_property = gst_red_filter_get_property;

  g_object_class_install_property (gobject_class, PROP_SILENT,
      g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
          FALSE, G_PARAM_READWRITE));

  gst_element_class_set_details_simple(gstelement_class,
    "RedFilter",
    "FIXME:Generic",
    "FIXME:Generic Template Element",
    " <<user@hostname.org>>");

  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sink_factory));
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_red_filter_init (GstRedFilter * filter)
{
  filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "sink");
  gst_pad_set_event_function (filter->sinkpad,
                              GST_DEBUG_FUNCPTR(gst_red_filter_sink_event));
  gst_pad_set_chain_function (filter->sinkpad,
                              GST_DEBUG_FUNCPTR(gst_red_filter_chain));
  GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
  gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

  filter->srcpad = gst_pad_new_from_static_template (&src_factory, "src");
  GST_PAD_SET_PROXY_CAPS (filter->srcpad);
  gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);

  filter->silent = FALSE;
}

static void
gst_red_filter_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstRedFilter *filter = GST_REDFILTER (object);

  switch (prop_id) {
    case PROP_SILENT:
      filter->silent = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_red_filter_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstRedFilter *filter = GST_REDFILTER (object);

  switch (prop_id) {
    case PROP_SILENT:
      g_value_set_boolean (value, filter->silent);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

/* GstElement vmethod implementations */

/* this function handles sink events */
static gboolean
gst_red_filter_sink_event (GstPad * pad, GstObject * parent, GstEvent * event)
{
  GstRedFilter *filter;
  gboolean ret;

  filter = GST_REDFILTER (parent);

  GST_LOG_OBJECT (filter, "Received %s event: %" GST_PTR_FORMAT,
      GST_EVENT_TYPE_NAME (event), event);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
    {
      GstCaps * caps;

      gst_event_parse_caps (event, &caps);
      /* do something with the caps */

      /* and forward */
      ret = gst_pad_event_default (pad, parent, event);
      break;
    }
    default:
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }
  return ret;
}

gdouble clamp(gdouble d) {
       if (d<0.0)   { d=0.0; }
  else if (d>255.0) { d=255.0; }

  return d;
}

gdouble YUV2R(gdouble y, gdouble v) {
  return clamp(1.164*(y-16)+1.596*(v-128));
}

gdouble R2Y(gdouble r) {
  return 0.257*r+16;
}

gdouble R2V(gdouble r) {
  return 0.439*r+128;
}

gdouble R2U(gdouble r) {
  return 128-0.148*r;
}

float msDelta(struct timespec *t0, struct timespec *t1)
{
  return (int64_t)(t1->tv_sec - t0->tv_sec) * 1000.0 +
         (int64_t)(t1->tv_nsec - t0->tv_nsec) / 1000000.0;
}

guint64 nsTime(struct timespec *t)
{
  return (guint64)(t->tv_sec) * (guint64)(1000000000UL) +
         (guint64)(t->tv_nsec);
}

gint64 tBase = 0xdeadbeefdeadbeef;
 
gint64 nsDelta(struct timespec *t)
{
  if (tBase == 0xdeadbeefdeadbeef)
  {
    tBase = nsTime(t);
  }

  return nsTime(t) - tBase;
}

/* chain function
 * this function does the actual processing
 */
static GstFlowReturn
gst_red_filter_chain (GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  GstRedFilter *filter;

  filter = GST_REDFILTER (parent);

  if (filter->silent == FALSE) {
    GstCaps *caps;
    GstStructure *struc = NULL;
    gint i, w, h;
    const char * fmt = NULL;
    struct timespec t0, t1;

    clock_gettime(CLOCK_REALTIME, &t0);

    caps = gst_pad_get_current_caps (pad);
    assert(caps);

    for (i = 0; i < gst_caps_get_size (caps); i++) {
      GstStructure *structure = gst_caps_get_structure (caps, i);
      if (gst_structure_has_name(structure, "video/x-raw")) {
        struc = structure;
        break;
      }
    }
    assert(struc);
    gst_caps_unref (caps);

    assert( gst_structure_get_int(struc, "width", &w) );
    assert( gst_structure_get_int(struc, "height", &h) );
    assert( (fmt = gst_structure_get_string(struc, "format")) );

    if (strcmp(fmt,"I420")==0)
    {
      GstMapInfo info;
      guint8 *y,*u,*U,*v;
      gdouble rlt,rrt,rlb,rrb,r;

      gst_buffer_map (buf, &info, GST_MAP_READWRITE);
      assert(w*h*3 == info.size*2);

      U=info.data+w*h;
      for(i=0, y=info.data, u=U, v=U+w*h/4; y<U; i+=2, y+=2, ++u, ++v) {
        if (i==w) { i=0; y+=w; if (y==U) break; }

        rlt = YUV2R(y[0+0],*v);
        rrt = YUV2R(y[0+1],*v);
        rlb = YUV2R(y[w+0],*v);
        rrb = YUV2R(y[w+1],*v);

        y[0+0] = R2Y(rlt);
        y[0+1] = R2Y(rrt);
        y[w+0] = R2Y(rlb);
        y[w+1] = R2Y(rrb);


        r = (rlt+rrt+rlb+rrb)/4.0;

        *u = R2U(r);
        *v = R2V(r);
      }
      assert(y==U);
      assert(u==U+w*h/4);
      assert(v==U+w*h/2);

      gst_buffer_unmap (buf, &info);

      clock_gettime(CLOCK_REALTIME, &t1);

#ifdef __x86_64__
      g_print("redfilter: %dx%d(%s) buf->pts=%ldns %.02fms real=%ldns\n", w, h, fmt, buf->pts, msDelta(&t0, &t1), nsDelta(&t0));
#else
      g_print("redfilter: %dx%d(%s) buf->pts=%lldns %.02fms real=%lldns\n", w, h, fmt, buf->pts, msDelta(&t0, &t1), nsDelta(&t0));
#endif
    } else if (strcmp(fmt,"RGB")==0) {
      GstMapInfo info;
      guint8 *p;

      gst_buffer_map (buf, &info, GST_MAP_READWRITE);
      assert(w*h*3 == info.size);

      for(p=info.data+info.size; p>info.data; ) {
        *--p=0; // b
        *--p=0; // g
        --p;    // r
      }
      assert(p==info.data);

      gst_buffer_unmap (buf, &info);

      clock_gettime(CLOCK_REALTIME, &t1);

#ifdef __x86_64__
      g_print("redfilter: %dx%d(%s) buf->pts=%ldns %.02fms real=%ldns\n", w, h, fmt, buf->pts, msDelta(&t0, &t1), nsDelta(&t0));
#else
      g_print("redfilter: %dx%d(%s) buf->pts=%lldns %.02fms real=%lldns\n", w, h, fmt, buf->pts, msDelta(&t0, &t1), nsDelta(&t0));
#endif
    } else {
      assert(!"unhandled redfilter format");
    }
  }

  /* just push out the incoming buffer without touching it */
  return gst_pad_push (filter->srcpad, buf);
}


/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
redfilter_init (GstPlugin * redfilter)
{
  /* debug category for fltering log messages
   *
   * exchange the string 'Template redfilter' with your description
   */
  GST_DEBUG_CATEGORY_INIT (gst_red_filter_debug, "redfilter",
      0, "Template redfilter");

  return gst_element_register (redfilter, "redfilter", GST_RANK_NONE,
      GST_TYPE_REDFILTER);
}

/* PACKAGE: this is usually set by autotools depending on some _INIT macro
 * in configure.ac and then written into and defined in config.h, but we can
 * just set it ourselves here in case someone doesn't use autotools to
 * compile this code. GST_PLUGIN_DEFINE needs PACKAGE to be defined.
 */
#ifndef PACKAGE
#define PACKAGE "myfirstredfilter"
#endif

/* gstreamer looks for this structure to register redfilters
 *
 * exchange the string 'Template redfilter' with your redfilter description
 */
GST_PLUGIN_DEFINE (
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    redfilter,
    "Template redfilter",
    redfilter_init,
    PACKAGE_VERSION,
    GST_LICENSE,
    GST_PACKAGE_NAME,
    GST_PACKAGE_ORIGIN
)
