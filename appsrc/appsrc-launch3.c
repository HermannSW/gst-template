#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <stdlib.h>
#include <unistd.h>

static GMainLoop *loop;
static gint fps, cnt=0;

static void
cb_fps_measurements(GstElement *fpsdisplaysink,
                    gdouble arg0,
                    gdouble arg1,
                    gdouble arg2,
                    gpointer user_data)
{
  g_print("dropped: %.0f, current: %.2f, average: %.2f\n", arg1, arg0, arg2);
}

gboolean
read_data (gpointer    user_data)
{
  static gboolean white = FALSE;
  static GstClockTime timestamp = 0;
  GstBuffer *buffer;
  guint size;
  GstFlowReturn ret;
  GstAppSrc *appsrc = user_data;

  size = 384 * 288 * 2;

  buffer = gst_buffer_new_allocate (NULL, size, NULL);

  /* this makes the image black/white */
  gst_buffer_memset (buffer, 0, white ? 0xff : 0x0, size);

  white = !white;

  GST_BUFFER_PTS (buffer) = timestamp;
  GST_BUFFER_DURATION (buffer) = gst_util_uint64_scale_int (1, GST_SECOND, fps);

  timestamp += GST_BUFFER_DURATION (buffer);

  ret = gst_app_src_push_buffer(appsrc, buffer);

  if (ret != GST_FLOW_OK) {
    /* something wrong, stop pushing */
    g_main_loop_quit (loop);
  }

  if (++cnt % 100 == 0)
    g_print("read_data(%d)\n",cnt);

  usleep(1000000/fps);

  return G_SOURCE_CONTINUE;
}

gint
main (gint   argc,
      gchar *argv[])
{
  GstElement *pipeline, *appsrc, *fpsdisplaysink;

  /* init GStreamer */
  gst_init (&argc, &argv);
  loop = g_main_loop_new (NULL, FALSE);

  /* setup pipeline */
  g_assert(argc == 3);
  fps = atoi(argv[1]); 
  pipeline = gst_parse_launch(argv[2], NULL); 
  g_assert(pipeline);

  /* setup */
  fpsdisplaysink = gst_bin_get_by_name (GST_BIN(pipeline), "#");
  appsrc = gst_bin_get_by_name (GST_BIN(pipeline), "_");
  g_assert(appsrc);
  g_object_set (G_OBJECT (appsrc), "caps",
        gst_caps_new_simple ("video/x-raw",
                     "format", G_TYPE_STRING, "RGB16",
                     "width", G_TYPE_INT, 384,
                     "height", G_TYPE_INT, 288,
                     "framerate", GST_TYPE_FRACTION, 0, 1,
                     NULL), NULL);

  /* setup appsrc */
  g_object_set (G_OBJECT (appsrc),
        "stream-type", 0,
        "format", GST_FORMAT_TIME, NULL);

  /* setup fpsdisplaysink "#" if present */
  if (fpsdisplaysink) {
    g_object_set (G_OBJECT (fpsdisplaysink), "signal-fps-measurements", TRUE, NULL);
    g_signal_connect (fpsdisplaysink, "fps-measurements", G_CALLBACK (cb_fps_measurements), NULL);
    g_print("fps-measurements connected\n");
  }

  g_idle_add ((GSourceFunc) read_data, appsrc);

  /* play */
  gst_element_set_state (pipeline, GST_STATE_PLAYING);
  g_main_loop_run (loop);

  /* clean up */
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (GST_OBJECT (pipeline));
  g_main_loop_unref (loop);

  return 0;
}
